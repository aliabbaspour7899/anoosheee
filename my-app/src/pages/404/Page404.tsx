import React from 'react'

const Page404:React.FC = () => {
    return (
        <div>
            <h1>404</h1>
            <p>ما پیدا نکردیم اون صفحه ای رو که دنبالش هستی ):</p>
        </div>
    )
}

export default Page404;
