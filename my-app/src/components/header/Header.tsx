import { Avatar, Menu, MenuItem, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import useStyLes from './styles'

const Header: React.FC = () => {
    const cleases = useStyLes();
    const [anchorEl, setAnchorEl] = useState(null);
    const handleClick = (event:any) => {
        setAnchorEl(event.currentTarget);
      };
    
      const handleClose = () => {
        setAnchorEl(null);
      };
    return (
        <div className={cleases.root} >
            <Typography>ali abbaspour</Typography>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" className={cleases.avatar} onClick={handleClick} />
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
                <MenuItem onClick={handleClose}>Logout</MenuItem>
            </Menu>
        </div>
    )
}

export default Header;
