import {makeStyles} from "@material-ui/styles";
const useStyLes = makeStyles(theme=>({
       root: {
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              padding: '10px 30px',
              boxShadow: '0 1px 5px -1px #8989894f'
       },
       avatar: {
              marginRight: '10px'
       },
}));

export default useStyLes;