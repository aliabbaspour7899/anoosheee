
import React from 'react';
import Layout from './layout/Layout';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Page404 from '../pages/404/Page404';
import Home from '../pages/home/Home';




const App: React.FC = () => {

    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route path={"/"} render={()=>{
                        return (
                            <Layout>
                                <Switch>
                                    <Route exact path={"/"} component={Home} />
                                    <Route exact path={"/dashboard"} component={Home} />
                                    <Route component={Page404} />
                                </Switch>
                            </Layout>
                        )
                    }}/>
                </Switch>
            </BrowserRouter>
        </div>
    )
}

export default App;
