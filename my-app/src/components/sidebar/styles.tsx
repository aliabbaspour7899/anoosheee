import {createStyles, makeStyles, Theme } from "@material-ui/core";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
       root: {
              width: '250px',
              backgroundColor: '#eee',
              height: '100vh'
              },
              nested: {
              paddingLeft: theme.spacing(4),
              },
  }),
);

export default useStyles;
