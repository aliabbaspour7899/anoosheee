import React from 'react'
import { Collapse, List, ListItem, ListItemIcon, ListItemText, ListSubheader } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PaymentIcon from '@material-ui/icons/Payment';
import ReceiptIcon from '@material-ui/icons/Receipt';
import WorkIcon from '@material-ui/icons/Work';
import DvrIcon from '@material-ui/icons/Dvr';
import PeopleIcon from '@material-ui/icons/People';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import ApartmentIcon from '@material-ui/icons/Apartment';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import useStyLes from './styles';  
import { Link } from 'react-router-dom';

const SaideBar:React.FC = () => {
    const classes= useStyLes();
    const [open, setOpen] = React.useState(false);
  
    const handleClick = () => {
      setOpen(!open);
    };
    return (
        <div>
               <List
                component="nav"
                aria-labelledby="nested-list-subheader"
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                    منوی حسابداری انوشه
                    </ListSubheader>
                }
                className={classes.root}
                >
                <Link to={'dashboard'}>
                    <ListItem button>
                        <ListItemIcon>
                        <DashboardIcon />
                        </ListItemIcon>
                        <ListItemText primary="داشبورد" />
                    </ListItem>
                </Link>
                <ListItem button onClick={handleClick}>
                    <ListItemIcon>
                    <ApartmentIcon />
                    </ListItemIcon>
                    <ListItemText primary="تفضیلی" />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Link to={'/asd'}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            <PeopleIcon />
                            </ListItemIcon>
                            <ListItemText primary="اشخاص" />
                        </ListItem>
                        </List>
                    </Collapse>
                </Link>
                <Link to={'/asd'}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            <WorkIcon />
                            </ListItemIcon>
                            <ListItemText primary="پروژه" />
                        </ListItem>
                        </List>
                    </Collapse>
                </Link>
                <Link to={'/asd'}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            <DvrIcon />
                            </ListItemIcon>
                            <ListItemText primary="کالا" />
                        </ListItem>
                        </List>
                    </Collapse>
                </Link>
                <Link to={'asddf'}>
                    <ListItem button>
                        <ListItemIcon>
                        <AccountTreeIcon />
                        </ListItemIcon>
                        <ListItemText primary="بابت ها" />
                    </ListItem>
                </Link>
                <Link to={'asddf'}>
                    <ListItem button>
                        <ListItemIcon>
                        <PaymentIcon />
                        </ListItemIcon>
                        <ListItemText primary="پرداخت ها" />
                    </ListItem>
                </Link>
                <Link to={'asddf'}>
                    <ListItem button>
                        <ListItemIcon>
                        <ReceiptIcon />
                        </ListItemIcon>
                        <ListItemText primary="دریافت ها" />
                    </ListItem>
                </Link>
                <Link to={'asddf'}>
                    <ListItem button>
                        <ListItemIcon>
                        <AssignmentIndIcon />
                        </ListItemIcon>
                        <ListItemText primary="حساب کاربری" />
                    </ListItem>
                </Link>
            </List>
        </div>
    )
}

export default SaideBar;
