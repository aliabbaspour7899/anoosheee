import {createStyles, makeStyles, Theme } from "@material-ui/core";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
       root: {
              display: 'flex',
              flexDirection: 'row',
       },
       base:{
              flex:1
       },
       nested: {
              paddingLeft: theme.spacing(4),
       },
  }),
);

export default useStyles;
