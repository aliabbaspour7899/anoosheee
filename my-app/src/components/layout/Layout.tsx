import React from 'react'
import Header from '../header/Header';
import SaideBar from '../sidebar/SaideBar';
import useStyLes from './styles'

const Layout: React.FC = ({children}) => {
    const classes = useStyLes();
    return (
        <div className={classes.root}>
            <SaideBar/>
            <div className={classes.base}>
                <Header/>
                {children}
            </div>
        </div>
    )
}

export default Layout;
