import {createMuiTheme} from "@material-ui/core";

const colorPrimary = "#5ea9dd";

const theme  =  createMuiTheme({
    palette: {
        primary: {
            main: colorPrimary,
        }
    },
    typography: {
        fontFamily:'IRANSans'
    },
    overrides:{
        MuiButton:{
            label:{
                fontFamily:'IRANSans'
            }
        }
    }
})

export default theme;
